import vibe.utils.dictionarylist;
import std.stdio;
import std.uuid;
import std.string;
import std.conv;
import vibe.http.server;
import vibe.http.router;
import core.time;
import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.client;
import std.datetime.systime;
import std.file;
import std.net.curl;
import std.json;
import std.ascii : LetterCase;
import std.digest;
import std.digest.md;
import std.digest.sha;
import std.string : representation;
import std.digest.hmac;
import std.base64;
import vibe.data.serialization;
import std.uri;
import mongodb;

Database db;

void main()
{
	db = new Database("accounting", "", "");

	auto router = new URLRouter;
	router
		.post("/writeNewClients",&writeNewClients)
		.post("/allClients",&allClients)
		.post("/getClient",&getClientsByNumber)
		.get("/deleteClients",&deleteClients);
	auto l = listenHTTP("0.0.0.0:8232", router);
	runApplication();
	scope (exit) {
	    exitEventLoop(true);
	    sleep(1.msecs);
    }
    l.stopListening();
}

void writeNewClients(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	long chat_id = req_end.json["chat_id"].to!long;
	auto clients = req_end.json["clients"];
	long total = clients.length, success = 0, duplicate = 0;

	for(int i = 0; i < clients.length; i++)
	{
		Json curClient = clients[i];
		curClient["chat_id"] = chat_id;

		requestHTTP("http://www.mega-bot.com/bots/push/19139301957e2175129a4b539fa81757",					
			(scope req) {
				req.method = HTTPMethod.POST;
				req.writeJsonBody(curClient);
			},
			(scope res){
			}
		);
		success++;
	}
	res_end.writeJsonBody(["total": total, "success": success, "duplicate": duplicate]);
}

void allClients(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	string chat_id = req_end.json["chat_id"].to!string;

	res_end.writeJsonBody(["clients": db["workspace_clients"].find("chat_id", chat_id.to!string)]);
}

void getClientsByNumber(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	res_end.writeJsonBody(["client": db["workspace_clients"].find("", req_end.json.toString())[0]]);
}

void deleteClients(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	db["workspace_clients"].remove("", "");
}
